# Arduino ESP8266开发板离线包安装指南

本仓库提供了一个用于Arduino IDE的ESP8266开发板离线安装包，包含以下4个文件：

1. `esp8266-3.1.2`
2. `i686-w64-mingw32.mklittlefs-30b7fc1.220621`
3. `i686-w64-mingw32.mkspiffs-7fefeac.220621`
4. `i686-w64-mingw32.xtensa-lx106-elf-e5f9fec.220621`

## 安装步骤

### 1. 下载文件
首先，从本仓库下载上述4个文件。

### 2. 复制文件到Arduino IDE目录
将下载的文件复制到Arduino IDE的安装目录。你可以通过以下两种方式找到该目录：

- 在Arduino IDE中，点击`文件` -> `首选项`，在弹出的窗口中找到`文件路径`。
- 直接进入目录：`C:\Users\你的计算机名字\AppData\Local\Arduino15`

### 3. 创建文件夹结构
在Arduino IDE的安装目录下，创建以下文件夹结构：

```
staging
└── packages
```

### 4. 复制文件到指定位置
将下载的4个文件复制到`staging/packages`文件夹中。

### 5. 配置Arduino IDE
在Arduino IDE中，点击`文件` -> `首选项`，在`附加开发板管理器网址`中填写以下URL：

```
https://arduino.esp8266.com/stable/package_esp8266com_index.json
```

### 6. 安装ESP8266开发板
打开`工具` -> `开发板` -> `开发板管理器`，搜索`esp8266`，然后选择`esp8266 by ESP8266 Community`，版本选择`3.1.2`，点击安装。

## 注意事项
- 使用离线包安装ESP8266开发板的速度比在线安装要快得多。
- 确保文件路径和文件夹结构正确，否则可能导致安装失败。

## 贡献
如果你有任何改进建议或发现了问题，欢迎提交Issue或Pull Request。

## 许可证
本项目采用MIT许可证，详情请参阅[LICENSE](LICENSE)文件。